// jshint esversion:6
// temporary file for storing modules

const page = {
    width: window.innerWidth,
    height: window.innerHeight,
    Update: function() {
        this.width = window.innerWidth;
        this.height = window.innerHeight;
    }
};
const MathC = {
    RandomRange: function($min = 0, $max = 1000) {
        // returns an integer between and including $min and $max
        return Math.floor(Math.random() * ($max - $min + 1) + $min);
    },
    Round: function($number, $precision = 0) {
        // returns rounded number of $number with precision of $precision
        return Math.round($number * Math.pow(10, $precision)) / Math.pow(10, $precision);
    }
};
const canvas = {
    element: ByClass('canv'),
    content: ByClass('canv').getContext('2d'),
    Clear: function() {
        this.content.clearRect(0, 0, page.width, page.height);
    },
    Maximize: function() {
        page.Update();
        this.element.width = page.width;
        this.element.height = page.height;
    },
};

const time = {
    pageLoad: performance.now(),
    frame: {
        start: null,
        end: null,
        delta: null,
        fps: null,
        current: 0,
        physics: {
            start: null,
            end: null,
        },
        draw: {
            start: null,
            end: null,
        },
    },
    Physics: function() {
        return this.frame.physics.end - this.frame.physics.start;
    },
    Draw: function() {
        return this.frame.draw.end - this.frame.draw.start;
    },
    Frame: function() {
        return this.frame.end - this.frame.start;
    },
    Delta: function() {},
    Iterate: function() {
        this.frame.delta = performance.now() - this.frame.start;
        this.frame.fps = 1000 / this.frame.delta;
        this.frame.current++;
    }
};
