// jshint esversion:6
// jshint -W138

class Text {
  constructor($text = 'undefined', $positionX = 30, $positionY = 30, $color = '#FFFFFF', $size = 20, $style = 'monospace') {
    this.text = $text;
    this.positionX = $positionX;
    this.positionY = $positionY;
    this.color = $color;
    this.size = $size + 'px';
    this.style = $style;
    this.font = this.size + ' ' + this.style;
  }
  Draw() {
    canvas.content.font = this.font;
    canvas.content.fillStyle = this.color;
    canvas.content.fillText(this.text, this.positionX, this.positionY);
  }
}

class TextBox {
  constructor($options = ['||opt0', '||opts1', '||opt2'], $positionX = 0, $positionY = 0, $scale = 0) {
    this.options = $options;
    this.positionX = $positionX;
    this.positionY = $positionY;
    this.scale = $scale;
    this.fontSize = 16 + this.scale;
    this.width = this._width + this._paddingX;
    this.height = this._height + this._paddingY;
  }
  get _width() {
    return this.CalculateWidth();
  }
  get _height() {
    return this.CalculateHeight();
  }
  get _paddingX() {
    return this.fontSize;
  }
  get _paddingY() {
    return this.fontSize / 2;
  }
  get _fontStyle() {
    return this.fontSize + 'px monospace';
  }
  get _fontColor() {
    return '#FFFFFF';
  }
  get _boxColor() {
    return 'rgba(255, 255, 255, 0.08)';
  }

  CalculateWidth() {
    let $width = 0;
    canvas.content.font = this._fontStyle;
    for (let i = 0; i < this.options.length; i++) {
      if (canvas.content.measureText(this.options[i]).width > $width) {
        $width = canvas.content.measureText(this.options[i]).width;
      }
    }
    return $width;
  }
  CalculateHeight() {
    return this.fontSize * this.options.length;
  }

  Draw() {
    this.DrawBox();
    this.DrawText();
  }

  DrawBox() {
    canvas.content.fillStyle = this._boxColor;
    canvas.content.fillRect(this.positionX, this.positionY, this.width, this.height);
  }
  DrawText() {
    canvas.content.font = this._fontStyle;
    canvas.content.fillStyle = this._fontColor;
    for (let i = 0; i < this.options.length; i++){
      canvas.content.fillText(this.options[i], this._paddingX / 2 + this.positionX, this.positionY + (this.fontSize * (i + 1)));
    }
  }
}

class Vertex {
  constructor($x, $y) {
    this.x = $x;
    this.y = $y;
  }
}

class Triangle {
  constructor($v1 = new Vertex(), $v2 = new Vertex(), $v3 = new Vertex()) {
    this.v1 = $v1;
    this.v2 = $v2;
    this.v3 = $v3;
    this.highlightVertex = true;
  }

  Draw() {
    canvas.content.strokeStyle = 'red';
    canvas.content.beginPath();
    canvas.content.moveTo(this.v1.x, this.v1.y);
    canvas.content.lineTo(this.v2.x, this.v2.y);
    canvas.content.lineTo(this.v3.x, this.v3.y);
    canvas.content.lineTo(this.v1.x, this.v1.y);
    canvas.content.closePath();
    canvas.content.stroke();
  }
}
