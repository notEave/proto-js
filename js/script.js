// jshint esversion:6
// jshint -W138

function ByID($domId) {
  return document.getElementById($domId);
}
function ByClass($className, $index = 0) {
  return document.getElementsByClassName($className)[$index];
}
const page = {
  width: window.innerWidth,
  height: window.innerHeight,
  Update: function() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
  }
};
const MathC = {
  RandomRange: function($min = 0, $max = 1000) {
    // returns an integer between and including $min and $max
    return Math.floor(Math.random() * ($max - $min + 1) + $min);
  },
  Round: function($number, $precision = 0) {
    // returns rounded number of $number with precision of $precision
    return Math.round($number * Math.pow(10, $precision)) / Math.pow(10, $precision);
  },
  Average: function($array = []) {
    let total = 0;
    for ( let i = 0; i < $array.length; i++ ) {
      total += $array[i];
    }
    return total / $array.length;
  }
};
const canvas = {
  element: ByClass('canv'),
  content: ByClass('canv').getContext('2d'),
  Clear: function() {
    this.content.clearRect(0, 0, page.width, page.height);
  },
  Maximize: function() {
    page.Update();
    this.element.width = page.width;
    this.element.height = page.height;
  },
};
const time = {
  pageLoad: performance.now(),
  frame: {
    start: null,
    end: null,
    delta: null,
    current: 0,
    physics: {
      start: null,
      end: null,
    },
    draw: {
      start: null,
      end: null,
    },
    fps: {
      current: null,
      array: [],
      average: null,
      Average: function() {
        this.array.push(this.current);
        while ( this.array.length > 2048) {
            this.array.shift();
        }
        this.average = MathC.Average(this.array);
        console.log(this.average);
        console.log(this.array.length);
      }
    },
  },
  Physics: function() {
    return this.frame.physics.end - this.frame.physics.start;
  },
  Draw: function() {
    return this.frame.draw.end - this.frame.draw.start;
  },
  Frame: function() {
    return this.frame.end - this.frame.start;
  },
  Delta: function() {},
  Iterate: function() {
    this.frame.delta = performance.now() - this.frame.start;
    this.frame.fps.current = 1000 / this.frame.delta;
    this.frame.fps.Average();
    this.frame.current++;
  }
};

function Start() {
  document.addEventListener('keydown', KeyboardEvent, false);
  document.addEventListener('click', ClickEvent, false);
  canvas.Maximize();
}
function Resize() {
  canvas.Maximize();
}
function KeyboardEvent(event) {}
function ClickEvent(event) {}

function Frame() {
  time.Iterate();
  time.frame.start = performance.now();

  Physics();
  Draw();

  time.frame.end = performance.now();
  requestAnimationFrame(Frame);
}

function Physics() {
  time.frame.physics.start = performance.now();

  time.frame.physics.end = performance.now();
}

function Draw() {
  time.frame.draw.start = performance.now();
  canvas.Clear();
  time.frame.draw.end = performance.now();
  new TextBox(['Physicstime: ' + MathC.Round(time.Physics()) + 'ms', 'Drawtime: ' + MathC.Round(time.Draw()) + 'ms', 'Framerate: ' + MathC.Round(time.frame.fps.current), 'Frames: ' + time.frame.current, 'Average fps: ' + MathC.Round(time.frame.fps.average, 1)], page.width / 2, page.height / 2).Draw();
  new Triangle(new Vertex(100, 200), new Vertex(200, 100), new Vertex( 300, 150)).Draw();
}

requestAnimationFrame(Frame);
